//
//  ViewController.swift
//  Set
//
//  Created by Rostislav Zapolsky on 4/23/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

class SetGameViewController: UIViewController, SetGridViewDelegate {
    
    // MARK: Variables
    var game: SetGame?
    var cardsAppeared: [SetCard] = []
    var oldCardsAppeared = [SetCard]()
    
    lazy var animator = UIDynamicAnimator(referenceView: view)
    lazy var cardBehavior = SetCardBehavior()
    
    @IBOutlet weak var cardGridView: SetGridView!
    @IBOutlet weak var dealCardsButton: UIButton!
    @IBOutlet weak var setButton: UIButton!
    @IBOutlet weak var deckStackView: UIStackView!
    @IBOutlet weak var pointsLabel: UILabel!
    var flyingViews = [UIView]()
    
    // MARK: Overriden
    override func viewDidLoad() {
        super.viewDidLoad()
        cardGridView.delegate = self
        
        addGestureRecognizers()
        startNewGame()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        updateViewFromModel()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { _ in
            self.updateViewFromModel()
        })
    }
    
    // MARK: Delegated
    func touchCard(_ sender: UIButton) {
        guard let game = game,
            let indexOfTouchedCard = cardGridView.subviews.firstIndex(of: sender) else { return }
        
        game.touchCard(at: indexOfTouchedCard)
        updateViewFromModel()
        checkIfGameEnded()
    }
    
    func chooseCardBorderColor(of card: SetCard) -> UIColor {
        var buttonBorderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        guard let game = game else { return buttonBorderColor }
        if game.chosenCards.contains(card) {
            buttonBorderColor = game.chosenCardsQuantityEqualsSetQuantity()
                && game.isCardsAreSet() ? #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1) : #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        }
        return buttonBorderColor
    }
    
    // MARK: Actions
    @IBAction func touchStartGameButton() {
        startNewGame()
        updateViewFromModel()
    }
    
    @IBAction func touchDealCardsButton(_ sender: UIButton) {
        dealCards()
    }
    
    @IBAction func findSetButton(_ sender: UIButton) {
        guard let game = game else { return }
        
        game.findSet(withCardsSelecting: true)
        updateViewFromModel()
        checkIfGameEnded()
    }
    
    // MARK: Gestures
    @objc func reshuffleCardsEvent(_ sender: UIRotationGestureRecognizer) {
        guard let game = game,
            case .ended = sender.state else {
                return
        }
        game.reshuffleCards()
        updateViewFromModel()
    }
    
    @objc func dealCardsEvent(_ sender: UISwipeGestureRecognizer) {
        dealCards()
    }
    
    // MARK: Support
    func startNewGame() {
        cardsAppeared.removeAll()
        cardGridView.clearOldCardsData()
        game = SetGame()
    }
    
    func addGestureRecognizers() {
        let swipeGestureRecognizer = UISwipeGestureRecognizer(target: self,
                                                              action: #selector(dealCardsEvent(_:)))
        swipeGestureRecognizer.direction = .down
        let rotateGestureRecognizer = UIRotationGestureRecognizer(target: self,
                                                                  action: #selector(reshuffleCardsEvent(_:)))
        view.addGestureRecognizer(swipeGestureRecognizer)
        view.addGestureRecognizer(rotateGestureRecognizer)
    }
    
    func checkIfGameEnded() {
        guard let game = game else { return }
        
        if !game.findSet(withCardsSelecting: false) && game.remainingCards.isEmpty {
            game.updateGameAfterFindingSet()
            updateViewFromModel()
            finishGame(with: game.totalPoints)
        }
    }
    
    func dealCards() {
        guard let game = game else { return }
        
        game.dealCardsOnButtonTouch()
        updateViewFromModel()
    }
    
    func finishGame(with points: Int) {
        let alert = UIAlertController(title: "Game over!",
                                      message: "You scored \(points) points",
            preferredStyle: .alert)
        let action = UIAlertAction(title: "OK",
                                   style: .default,
                                   handler: { _ in
                                    self.startNewGame()
                                    self.updateViewFromModel()
        })
        present(alert, animated: true, completion: nil)
        alert.addAction(action)
    }
    
    // MARK: View update
    func updateViewFromModel() {
        guard let game = game else { return }
        cardGridView.oldGridData = cardGridView.gridData
        oldCardsAppeared = cardsAppeared
        
        cardGridView.cardsDeckCenter = CGPoint(x: dealCardsButton.frame.midX,
                                               y: deckStackView.frame.minY - deckStackView.frame.height)
        
        cardGridView.gridData.frame = cardGridView.bounds
        cardGridView.gridData.cellCount = game.ingameCards.count
        
        updateFlyingCards()
        
        for subview in cardGridView.subviews {
            subview.removeFromSuperview()
        }
        
        cardsAppeared.removeAll()
        
        cardGridView.updateGrid(with: game.ingameCards, from: oldCardsAppeared)
        
        cardsAppeared = game.ingameCards
        
        dealCardsButton.isEnabled = !game.remainingCards.isEmpty
        pointsLabel.text = "\(game.totalPoints)"
    }
    
    func updateFlyingCards() {
        guard let game = game else { return }
        
        for subview in flyingViews {
            subview.removeFromSuperview()
        }
        flyingViews.removeAll()
        
        for cardAppearedIndex in cardsAppeared.indices
            where game.ingameCards.firstIndex(of: cardsAppeared[cardAppearedIndex]) == nil {
                guard let cardAppeared = cardGridView.subviews[cardAppearedIndex] as? SetCardButton,
                    let cardFrame = cardGridView.gridData[0] else { return }
                flyingViews.append(cardAppeared)
                cardBehavior.addItem(cardAppeared)
                animator.addBehavior(cardBehavior)
                let push = UIPushBehavior(items: [cardAppeared], mode: .instantaneous)
                push.pushDirection = CGVector(dx: view.frame.midX, dy: view.frame.midY)
                push.magnitude = 10
                push.action = {
                    push.dynamicAnimator?.removeBehavior(push)
                }
                animator.addBehavior(push)
                flyingViews.append(cardAppeared)
                _ = Timer.scheduledTimer(withTimeInterval: SetCardBehavior.DefaultProperties.animationChaoticDuration,
                                         repeats: false) { _ in
                                            cardAppeared.startRecallAnimation(on: CGRect(
                                                x: self.setButton.frame.midX - cardFrame.width / 2,
                                                y: self.deckStackView.frame.midY - cardFrame.height / 2,
                                                width: cardFrame.width,
                                                height: cardFrame.height))
                }
        }
        
        for subview in flyingViews {
            view.addSubview(subview)
        }
    }
}
