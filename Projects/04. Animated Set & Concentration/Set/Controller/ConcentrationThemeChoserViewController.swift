//
//  ConcentrationThemeChoserViewController.swift
//  AnimatedSet
//
//  Created by Rostislav Zapolsky on 5/16/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

class ConcentrationThemeChoserViewController: UIViewController, UISplitViewControllerDelegate {
    
    // MARK: Properties
    var gameThemes = [ConcentrationTheme]()
    var detailViewController: ConcentrationGameViewController? {
        guard let split = self.splitViewController else { return nil }
        let controllers = split.viewControllers
        return controllers[controllers.count-1] as? ConcentrationGameViewController
    }
    
    @IBOutlet weak var themesStackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fillGameThemes()
        updateThemeButtons()
    }
    
    // MARK: Overriden
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil) { _ in
            self.updateThemeButtons()
        }
    }
    
    // MARK: Delegated
    override func awakeFromNib() {
        splitViewController?.delegate = self
    }
    
    // MARK: Support
    func splitViewController(
        _ splitViewController: UISplitViewController,
        collapseSecondary secondaryViewController: UIViewController,
        onto primaryViewController: UIViewController) -> Bool {
        if let cvc = secondaryViewController as? ConcentrationGameViewController {
            if cvc.currentGameTheme == nil {
                return true
            }
        }
        return false
    }
    
    func fillGameThemes() {
        gameThemes.append(ConcentrationTheme(name: "Halloween",
                                             emoji: ["👻", "🎃", "🦇", "😱", "🙀", "😈", "🍭", "🍬", "🍎", "😨"],
                                             withBackgroundColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), withAdditionalColor: #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)))
        gameThemes.append(ConcentrationTheme(name: "Animals",
                                             emoji: ["🐷", "🐔", "🐢", "🐶", "🐲", "🐇", "🐤", "🐸", "🐝", "🐠"],
                                             withBackgroundColor: #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1), withAdditionalColor: #colorLiteral(red: 0.1921568662, green: 0.007843137719, blue: 0.09019608051, alpha: 1)))
        gameThemes.append(ConcentrationTheme(name: "Sport",
                                             emoji: ["⚽️", "🥊", "🏉", "🏐", "🏓", "🛹", "⛸", "🥎", "🎱", "🏆"],
                                             withBackgroundColor: #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1), withAdditionalColor: #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)))
        gameThemes.append(ConcentrationTheme(name: "Emoji",
                                             emoji: ["🥺", "🤠", "🤡", "🤑", "🙄", "🤯", "🤕", "🤥", "😓", "🥴"],
                                             withBackgroundColor: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), withAdditionalColor: #colorLiteral(red: 0.3098039329, green: 0.2039215714, blue: 0.03921568766, alpha: 1)))
        gameThemes.append(ConcentrationTheme(name: "Nature",
                                             emoji: ["🌝", "🌚", "☄️", "💥", "💦", "🌈", "🌞", "❄️", "💫", "🌪"],
                                             withBackgroundColor: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), withAdditionalColor: #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1)))
        gameThemes.append(ConcentrationTheme(name: "Food",
                                             emoji: ["🥦", "🍆", "🍒", "🍑", "🍌", "🍉", "🍅", "🧀", "🍔", "🌭"],
                                             withBackgroundColor: #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1), withAdditionalColor: #colorLiteral(red: 0.1294117719, green: 0.2156862766, blue: 0.06666667014, alpha: 1)))
    }
    
    func updateThemeButtons() {
        
        for themesStackView in themesStackView.subviews {
            themesStackView.removeFromSuperview()
        }
        
        for gameThemeNumber in gameThemes.indices {
            
            let cardView = UIButton(type: .system)
            let spaceBetweenTheme: CGFloat = themesStackView.bounds.height / CGFloat(gameThemes.count) 
            
            cardView.frame = CGRect(x: themesStackView.bounds.minX,
                                    y: spaceBetweenTheme * CGFloat(gameThemeNumber),
                                    width: themesStackView.bounds.width,
                                    height: spaceBetweenTheme * 0.5)
            cardView.setTitle(gameThemes[gameThemeNumber].name, for: .normal)
            cardView.addTarget(self, action: #selector(changeTheme(_:)), for: .touchUpInside)
            
            themesStackView.addSubview(cardView)
        }
    }
    
    @objc func changeTheme(_ sender: Any) {
        guard let pressedButton = sender as? UIButton,
            let currentTitle = pressedButton.currentTitle,
            let chosenTheme = gameThemes.first(where: { $0.name == currentTitle }) else { return }
        
        guard let existedDetailViewController = detailViewController else {
            if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ConcentrationGameViewController") as? ConcentrationGameViewController {
                controller.currentGameTheme = gameThemes.first(where: { $0.name == currentTitle })
                navigationController?.pushViewController(controller, animated: true)
            }
            return
        }
        existedDetailViewController.currentGameTheme = chosenTheme
    }
}
