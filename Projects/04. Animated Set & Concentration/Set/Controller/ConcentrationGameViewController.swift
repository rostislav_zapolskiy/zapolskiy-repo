//
//  ViewController.swift
//  Concentration
//
//  Created by Rostislav Zapolsky on 4/19/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

class ConcentrationGameViewController: UIViewController {
    
    // MARK: Properties
    var game: ConcentrationGame?
    var currentGameTheme: ConcentrationTheme? {
        didSet {
            if cardButtons != nil {
                startNewGame()
            }
        }
    }
    var emoji = [Int: String]()
    
    // MARK: Outlets
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var flipCountLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet var cardButtons: [UIButton]!
    @IBOutlet weak var newGameButton: UIButton!
    
    // MARK: Overriden
    override func viewDidLoad() {
        super.viewDidLoad()
        startNewGame()
    }
    
    // MARK: Actions
    @IBAction func touchCard(_ sender: UIButton) {
        guard let cardNumber = cardButtons.firstIndex(of: sender) else { return }
        game?.chooseCard(at: cardNumber)
        updateViewFromModel()
    }
    
    @IBAction func touchStartNewGameButton() {
        startNewGame()
    }
    
    // MARK: Support
    func startNewGame() {
        updateGameTheme()
        game = ConcentrationGame(numberOfPairsOfCards: (cardButtons.count + 1) / 2)
        
        emoji.removeAll()
        
        updateViewFromModel()
    }
    
    func emoji(for card: ConcentrationCard) -> String {
        guard let currentGameTheme = currentGameTheme else { return "?" }
        if emoji[card.identifier] == nil, currentGameTheme.emoji.count > 0 {
            let randomIndex = Int.random(in: 0..<currentGameTheme.emoji.count)
            emoji[card.identifier] = currentGameTheme.emoji.remove(at: randomIndex)
        }
        
        return emoji[card.identifier] ?? "?"
    }
    
    // MARK: View update
    func updateViewFromModel() {
        guard let game = game, let currentGameTheme = currentGameTheme else { return }
        for index in cardButtons.indices {
            let button = cardButtons[index]
            let card = game.cards[index]
            
            if card.isFaceUp {
                button.setTitle(emoji(for: card), for: .normal)
                button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            } else {
                button.setTitle("", for: .normal)
                button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 0) : currentGameTheme.additionalColor
            }
        }
        flipCountLabel.text = "Flips: \(game.flipCount)"
        scoreLabel.text = "Score: \(game.score)"
    }
    
    func updateGameTheme() {
        guard let currentGameTheme = currentGameTheme else { return }
        backgroundView.backgroundColor = currentGameTheme.backgroundColor
        flipCountLabel.textColor = currentGameTheme.additionalColor
        scoreLabel.textColor = currentGameTheme.additionalColor
        newGameButton.setTitleColor(currentGameTheme.additionalColor, for: .normal)
        
        for cardButton in cardButtons {
            cardButton.backgroundColor = currentGameTheme.additionalColor
        }
        currentGameTheme.resetEmoji()
    }
}
