//
//   CGRect+Extensions.swift.swift
//  AnimatedSet
//
//  Created by Rostislav Zapolsky on 6/7/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

extension CGRect {
    func insetByDefaultGridIndent() -> CGRect {
        return self.insetBy(dx: SetGridView.defaultCardIndent, dy: SetGridView.defaultCardIndent)
    }
}
