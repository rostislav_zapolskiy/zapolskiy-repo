//
//  SetCardBehavior.swift
//  AnimatedSet
//
//  Created by Rostislav Zapolsky on 6/3/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

class SetCardBehavior: UIDynamicBehavior {
    
    var flyingViews = [UIView]()
    
    lazy var cardsCollisionBehavior: UICollisionBehavior = {
        let behavior = UICollisionBehavior()
        behavior.translatesReferenceBoundsIntoBoundary = true
        return behavior
    }()
    lazy var cardsItemBehavior: UIDynamicItemBehavior = {
        let behavior = UIDynamicItemBehavior()
        behavior.elasticity = 1
        behavior.resistance = 0
        return behavior
    }()
    
    override init() {
        super.init()
        addChildBehavior(cardsCollisionBehavior)
        addChildBehavior(cardsItemBehavior)
    }
    
    func addItem(_ item: UIView) {
        cardsItemBehavior.addItem(item)
        cardsCollisionBehavior.addItem(item)
        
        _ = Timer.scheduledTimer(withTimeInterval: DefaultProperties.animationChaoticDuration,
                                 repeats: true) { _ in
                                    self.cardsItemBehavior.removeItem(item)
                                    self.cardsCollisionBehavior.removeItem(item)
        }
    }
}

extension SetCardBehavior {
    struct DefaultProperties {
        static let animationChaoticDuration: Double = 2
    }
}
