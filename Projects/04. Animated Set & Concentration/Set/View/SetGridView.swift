//
//  SetGridView.swift
//  AnimatedSet
//
//  Created by Rostislav Zapolsky on 5/29/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

protocol SetGridViewDelegate: class {
    func chooseCardBorderColor(of card: SetCard) -> UIColor
    func touchCard(_ sender: UIButton)
}

class SetGridView: UIView {
    
    lazy var gridData = SetGrid(layout: .aspectRatio(DefaultPropetries.cardAspectRatio), frame: bounds)
    lazy var oldGridData = gridData
    
    var cardsDeckCenter: CGPoint?
    
    weak var delegate: SetGridViewDelegate?
    
    func clearOldCardsData() {
        for subview in subviews {
            subview.removeFromSuperview()
        }
    }
    
    func updateGrid(with cards: [SetCard], from oldCards: [SetCard]) {
        var indexOfAnimatingCard = 0
        var cardView: SetCardButton?
        
        for ingameCard in cards {
            let oldCellFrame: CGRect? = oldCards.firstIndex(of: ingameCard) == nil ? nil
                : oldGridData[oldCards.firstIndex(of: ingameCard)!]
            guard let newCellFrame = gridData[cards.firstIndex(of: ingameCard)!],
                let cardsDeckCenter = cardsDeckCenter else { return }
            
            if let oldCellFrame = oldCellFrame {
                cardView = SetCardButton(frame: oldCellFrame.insetByDefaultGridIndent())
                cardView?.isFaceUp = true
            } else {
                cardView = SetCardButton(frame: newCellFrame.insetByDefaultGridIndent())
                cardView?.center = cardsDeckCenter
            }
            
            guard var cardView = cardView else { return }
            cardView.addTarget(self, action: #selector(touchCardEvent(_:)), for: .touchUpInside)
            createCardView(for: ingameCard, with: &cardView)
            
            if oldCellFrame == nil {
                DispatchQueue.main.asyncAfter(deadline: .now() +
                    DefaultPropetries.animationCardDealingDelayBetween * Double(indexOfAnimatingCard)) {
                        cardView.startDealAnimation(on: newCellFrame)
                }
                indexOfAnimatingCard += 1
            } else {
                cardView.startMoveAnimation(on: newCellFrame.insetByDefaultGridIndent())
            }
            addSubview(cardView)
        }
    }
    
    func createCardView(for card: SetCard?, with cardView: inout SetCardButton) {
        guard let card = card,
            let buttonSymbol = card.dimensionIdentifiers[0].value as? Symbols,
            let buttonSymbolQuantity = card.dimensionIdentifiers[1].value as? Int,
            let buttonSymbolColor = card.dimensionIdentifiers[2].value as? UIColor,
            let buttonSymbolTransparency = card.dimensionIdentifiers[3].value as? Transparency else {
                cardView.setCardButtonBorder(borderColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                return
        }
        cardView.setCardButtonImage(symbol: buttonSymbol, quantity: buttonSymbolQuantity,
                                    color: buttonSymbolColor, transparency: buttonSymbolTransparency)
        
        if let buttonBorderColor = delegate?.chooseCardBorderColor(of: card) {
            cardView.setCardButtonBorder(borderColor: buttonBorderColor)
        }
    }
    
    @objc func touchCardEvent(_ sender: UIButton) {
        delegate?.touchCard(sender)
    }
}

extension SetGridView {
    private struct DefaultPropetries {
        static let cardAspectRatio: CGFloat = 8.0 / 5.0
        static let cardIndent: CGFloat = 0.5
        
        static let animationCardDealingDelayBetween = 0.2
    }
    
    static var defaultCardIndent: CGFloat {
        return DefaultPropetries.cardIndent
    }
}
