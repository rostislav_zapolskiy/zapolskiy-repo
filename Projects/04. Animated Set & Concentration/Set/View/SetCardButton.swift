//
//  CardButton.swift
//  Set
//
//  Created by Rostislav Zapolsky on 4/26/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

class SetCardButton: UIButton {
    
    // MARK: Properties
    var cardBorderColor = UIColor.black.cgColor { didSet { setNeedsDisplay() } }
    
    var symbol: Symbols? { didSet { setNeedsDisplay() } }
    var symbolColor: UIColor? { didSet { setNeedsDisplay() } }
    var symbolQuantity: Int? { didSet { setNeedsDisplay() } }
    var symbolTransparency: Transparency? { didSet { setNeedsDisplay() } }
    
    var isFaceUp = false { didSet { setNeedsDisplay() } }
    
    // MARK: Overriden
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.cornerRadius = cornerRadius
        layer.borderWidth = ButtonDefaultProperties.borderWidth
        layer.borderColor = cardBorderColor
        
        guard isFaceUp else {
        	layer.backgroundColor = UIColor.darkGray.cgColor
            return
        }
        drawFrontOfCard()
        layer.backgroundColor = UIColor.white.cgColor
    }
    
    // MARK: Setters
    func setCardButtonImage(symbol: Symbols, quantity: Int, color: UIColor, transparency: Transparency) {
        self.symbol = symbol
        symbolQuantity = quantity
        symbolColor = color
        symbolTransparency = transparency
    }
    
    func setCardButtonBorder(borderColor: UIColor) {
        cardBorderColor = borderColor.cgColor
    }
    
    // MARK: Drawing
    func drawFrontOfCard() {
        guard let symbol = symbol,
            let symbolColor = symbolColor,
            let symbolQuantity = symbolQuantity,
            let symbolTransparency = symbolTransparency else { return }
        
        symbolColor.setFill()
        symbolColor.setStroke()
        
        for symbolNumber in 0..<symbolQuantity {
            let path = UIBezierPath()
            let symbolXPosition = bounds.maxX / CGFloat(symbolQuantity + 1) * (CGFloat(symbolNumber + 1))
            path.lineWidth = ButtonDefaultProperties.figureLineWidth
            
            drawFigure(withSymbol: symbol, throughPath: path, withXPosition: symbolXPosition)
            
            switch symbolTransparency {
            case .solid:
                path.fill()
            case .striped:
                UIGraphicsGetCurrentContext()?.saveGState()
                path.addClip()
                for stripeNumber in 0..<Int(ButtonDefaultProperties.symbolStripesQuantity) {
                    path.move(to: CGPoint(
                        x: CGFloat(stripeNumber) * bounds.width / ButtonDefaultProperties.symbolStripesQuantity,
                        y: bounds.minY))
                    path.addLine(to: CGPoint(
                        x: CGFloat(stripeNumber) * bounds.width / ButtonDefaultProperties.symbolStripesQuantity,
                        y: bounds.maxY))
                }
                path.stroke()
                UIGraphicsGetCurrentContext()?.restoreGState()
            case .unfilled:
                path.stroke()
            }
        }
    }
    
    func drawFigure(withSymbol symbol: Symbols, throughPath path: UIBezierPath,
                    withXPosition symbolXPosition: CGFloat) {
        switch symbol {
        case .diamond:
            drawDiamond(throughPath: path, withXPosition: symbolXPosition)
        case .oval:
            drawOval(throughPath: path, withXPosition: symbolXPosition)
        case .squiggle:
            drawSquiggle(throughPath: path, withXPosition: symbolXPosition)
        }
    }
    
    func drawDiamond(throughPath path: UIBezierPath, withXPosition symbolXPosition: CGFloat) {
        path.move(to: CGPoint(x: symbolXPosition - diamondXIndent, y: bounds.midY))
        path.addLine(to: CGPoint(x: symbolXPosition, y: bounds.midY - diamondYIndent))
        path.addLine(to: CGPoint(x: symbolXPosition + diamondXIndent, y: bounds.midY))
        path.addLine(to: CGPoint(x: symbolXPosition, y: bounds.midY + diamondYIndent))
        path.close()
    }
    
    func drawOval(throughPath path: UIBezierPath, withXPosition symbolXPosition: CGFloat) {
        path.move(to: CGPoint(x: symbolXPosition - ovalXIndent, y: bounds.midY - ovalYIndent))
        path.addQuadCurve(to: CGPoint(x: symbolXPosition + ovalXIndent, y: bounds.midY - ovalYIndent),
                          controlPoint: CGPoint(x: symbolXPosition,
                                                y: bounds.midY - ovalYIndent - ovalYRoundIndent))
        path.addLine(to: CGPoint(x: symbolXPosition + ovalXIndent, y: bounds.midY + ovalYIndent))
        path.addQuadCurve(to: CGPoint(x: symbolXPosition - ovalXIndent, y: bounds.midY + ovalYIndent),
                          controlPoint: CGPoint(x: symbolXPosition,
                                                y: bounds.midY + ovalYIndent + ovalYRoundIndent))
        path.close()
    }
    
    func drawSquiggle(throughPath path: UIBezierPath, withXPosition symbolXPosition: CGFloat) {
        let squggleRect = CGRect(origin: CGPoint(x: symbolXPosition - squiggleWidthIndent,
                                                 y: bounds.midY - squiggleHeightIndent),
                                 size: CGSize(width: 2 * squiggleWidthIndent,
                                              height: 2 * squiggleHeightIndent))
        let xIndent = squggleRect.width * ButtonDefaultProperties.squiggleXIndentMultiplier
        let yIndent = squggleRect.height * ButtonDefaultProperties.squiggleYIndentMultiplier
        path.move(to: CGPoint(x: squggleRect.midX, y: squggleRect.maxY))
        path.addCurve(to: CGPoint(
            x: squggleRect.minX + squggleRect.width * ButtonDefaultProperties.squiggleXMultiplier,
            y: squggleRect.maxY - squggleRect.height * ButtonDefaultProperties.squiggleBigLoopYMultiplier),
                      controlPoint1: CGPoint(
                        x: squggleRect.minX,
                        y: squggleRect.maxY),
                      controlPoint2: CGPoint(
                        x: squggleRect.minX + squggleRect.width * ButtonDefaultProperties.squiggleXMultiplier - xIndent,
                        y: squggleRect.maxY - squggleRect.height *
                            ButtonDefaultProperties.squiggleBigLoopYMultiplier + yIndent))
        path.addCurve(to: CGPoint(
            x: squggleRect.minX + squggleRect.width * 2 * ButtonDefaultProperties.squiggleXMultiplier,
            y: squggleRect.minY + squggleRect.height * ButtonDefaultProperties.squiggleSmallLoopYMultiplier),
                      controlPoint1: CGPoint(
                        x: squggleRect.minX + squggleRect.width * ButtonDefaultProperties.squiggleXMultiplier + xIndent,
                        y: squggleRect.maxY - squggleRect.height *
                            ButtonDefaultProperties.squiggleBigLoopYMultiplier - yIndent),
                      controlPoint2: CGPoint(
                        x: squggleRect.minX + squggleRect.width * ButtonDefaultProperties.squiggleXMultiplier + xIndent,
                        y: squggleRect.minY + squggleRect.height *
                            ButtonDefaultProperties.squiggleSmallLoopYMultiplier + yIndent))
        path.addCurve(to: CGPoint(x: squggleRect.midX, y: squggleRect.minY),
                      controlPoint1: CGPoint(
                        x: squggleRect.minX + squggleRect.width * ButtonDefaultProperties.squiggleXMultiplier,
                        y: squggleRect.minY),
                      controlPoint2: CGPoint(
                        x: squggleRect.minX + squggleRect.width * ButtonDefaultProperties.squiggleXMultiplier,
                        y: squggleRect.minY))
        let rightSquillePart = UIBezierPath(cgPath: path.cgPath)
        rightSquillePart.apply(CGAffineTransform.identity.rotated(by: CGFloat.pi))
        rightSquillePart.apply(CGAffineTransform.identity.translatedBy(x: bounds.width - 0.5, y: bounds.height))
        path.append(rightSquillePart)
    }
    
    // MARK: Animations
    func startDealAnimation(on frame: CGRect) {
        UIViewPropertyAnimator.runningPropertyAnimator(
            withDuration: ButtonDefaultProperties.animationCardMovingDuration,
            delay: 0,
            options: .curveEaseInOut,
            animations: {
                self.center.x = frame.midX
                self.center.y = frame.midY
        },
            completion: { _ in
                UIView.transition(with: self,
                                  duration: ButtonDefaultProperties.animationCardRotation,
                                  options: .transitionFlipFromLeft,
                                  animations: {
                                    self.isFaceUp = true
                },
                                  completion: nil)
        })
    }
    
    func startMoveAnimation(on newFrame: CGRect) {
        UIViewPropertyAnimator.runningPropertyAnimator(
            withDuration: ButtonDefaultProperties.animationCardMovingDuration,
                                                       delay: 0,
                                                       options: .curveEaseInOut,
                                                       animations: {
                                                        self.center.x = newFrame.midX
                                                        self.center.y = newFrame.midY
                                                        self.frame = newFrame
        },
                                                       completion: nil)
    }
    
    func startRecallAnimation(on newFrame: CGRect) {
        UIViewPropertyAnimator.runningPropertyAnimator(
            withDuration: ButtonDefaultProperties.animationCardMovingDuration,
            delay: 0,
            options: .curveEaseInOut,
            animations: {
                self.center.x = newFrame.midX
                self.center.y = newFrame.midY
                self.transform = CGAffineTransform.identity.rotated(by: CGFloat.pi / 0.5)
                self.bounds = newFrame
        },
            completion: { _ in
                UIView.transition(with: self,
                                  duration: ButtonDefaultProperties.animationCardRotation,
                                  options: .transitionFlipFromLeft,
                                  animations: {
                                    self.isFaceUp = false
                },
                                  completion: { _ in
                                    self.alpha = 0
                })
        })
    }
}

extension SetCardButton {
    // MARK: Constants
    private struct ButtonDefaultProperties {
        static let borderWidth: CGFloat = 3
        static let figureLineWidth: CGFloat = 1
        static let symbolStripesQuantity: CGFloat = 25
        
        static let cornerRadiusToBoundsHeight: CGFloat = 1 / 10
        
        static let diamondXIndentMultiplier: CGFloat = 1 / 12
        static let diamondYIndentMultiplier: CGFloat = 1 / 4
        static let ovalXIndentMultiplier: CGFloat = 1 / 12
        static let ovalYIndentMultiplier: CGFloat = 1 / 4
        static let ovalYRoundIndentMultiplier: CGFloat = 1 / 8
        
        static let squiggleWidthMultiplier: CGFloat = 1 / 10
        static let squiggleHeightMultiplier: CGFloat = 1 / 3
        static let squiggleXMultiplier: CGFloat = 1 / 8
        static let squiggleBigLoopYMultiplier: CGFloat = 1 / 2
        static let squiggleSmallLoopYMultiplier: CGFloat = 1 / 5
        static let squiggleXIndentMultiplier: CGFloat = 1 / 5
        static let squiggleYIndentMultiplier: CGFloat = 1 / 10
        
        static let animationCardMovingDuration = 1.1
        static let animationCardRotation = 1.2
    }
    
    private var cornerRadius: CGFloat {
        return bounds.size.height * ButtonDefaultProperties.cornerRadiusToBoundsHeight
    }
    private var diamondXIndent: CGFloat {
        return bounds.maxX * ButtonDefaultProperties.diamondXIndentMultiplier
    }
    private var diamondYIndent: CGFloat {
        return bounds.maxY * ButtonDefaultProperties.diamondYIndentMultiplier
    }
    private var ovalXIndent: CGFloat {
        return bounds.maxX * ButtonDefaultProperties.ovalXIndentMultiplier
    }
    private var ovalYIndent: CGFloat {
        return bounds.maxY * ButtonDefaultProperties.ovalYIndentMultiplier
    }
    private var ovalYRoundIndent: CGFloat {
        return bounds.maxY * ButtonDefaultProperties.ovalYRoundIndentMultiplier
    }
    private var squiggleWidthIndent: CGFloat {
        return bounds.maxX * ButtonDefaultProperties.squiggleWidthMultiplier
    }
    private var squiggleHeightIndent: CGFloat {
        return bounds.maxY * ButtonDefaultProperties.squiggleHeightMultiplier
    }
}
