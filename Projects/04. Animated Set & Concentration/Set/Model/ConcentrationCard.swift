//
//  Card.swift
//  Concentration
//
//  Created by Rostislav Zapolsky on 4/19/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import Foundation

struct ConcentrationCard {
    var isFaceUp = false
    var isMatched = false
    var identifier: Int
    
    static var identifierFactory = 0
    
    init() {
        self.identifier = ConcentrationCard.getUniqueIdentifier()
    }
    
    static func getUniqueIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
}
