//
//  Card.swift
//  Set
//
//  Created by Rostislav Zapolsky on 4/23/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import Foundation

struct SetCard: Hashable {
    let dimensionIdentifiers: [SetDimension]
    
    init(dimensionIdentifiers: [SetDimension]) {
        self.dimensionIdentifiers = dimensionIdentifiers
    }
    
    static func == (lhs: SetCard, rhs: SetCard) -> Bool {
        return lhs.dimensionIdentifiers == rhs.dimensionIdentifiers
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(dimensionIdentifiers)
    }
}
