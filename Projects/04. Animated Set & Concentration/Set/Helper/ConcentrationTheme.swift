//
//  Theme.swift
//  Concentration
//
//  Created by Rostislav Zapolsky on 4/22/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

class ConcentrationTheme {
    
    var name: String
    var emoji: [String]
    var defaultEmoji: [String]
    var backgroundColor: UIColor
    var additionalColor: UIColor
    
    init(name: String, emoji: [String], withBackgroundColor backgroundColor: UIColor,
         withAdditionalColor additionalColor: UIColor) {
        self.name = name
        self.emoji = emoji
        self.defaultEmoji = emoji
        self.backgroundColor = backgroundColor
        self.additionalColor = additionalColor
    }
    
    func resetEmoji() {
        emoji = defaultEmoji
    }
}
