//
//  ImageSetup.swift
//  MyPersonalPage
//
//  Created by Rostislav Zapolsky on 4/10/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import Foundation
import UIKit

class ProfilePhotoImageView: UIImageView {
  override init(image: UIImage?) {
    super.init(image: image)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    updateImageSettings()
  }
  
  func updateImageSettings() {
    self.layer.borderWidth = 1.0
    self.layer.masksToBounds = false
    self.layer.borderColor = UIColor.black.cgColor
    self.layer.cornerRadius = self.frame.size.width / 2
    self.clipsToBounds = true
  }
}
