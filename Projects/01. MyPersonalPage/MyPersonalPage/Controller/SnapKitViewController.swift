//
//  SnapKitViewController.swift
//  MyPersonalPage
//
//  Created by Rostislav Zapolsky on 4/18/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit
import SnapKit

class SnapKitViewController: UIViewController {
  var photoImageView: ProfilePhotoImageView!
  var nameTextView: UITextView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    createSubviews()
    setDefaultConstraints()
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    photoImageView.snp.removeConstraints()
    nameTextView.snp.removeConstraints()
    setDefaultConstraints()
  }
  
  func createSubviews() {
    photoImageView = ProfilePhotoImageView(image: UIImage(named: "my_photo@2x")!)
    self.view.addSubview(self.photoImageView)
    
    nameTextView = UITextView(frame: CGRect(x: 35, y: 366.5, width: 250, height: 55))
    nameTextView.text = """
    Rostislav Zapolskiy
    Trainee iOS Developer
    """
    nameTextView.textAlignment = NSTextAlignment.center
    nameTextView.textColor = UIColor.black
    nameTextView.font = UIFont.systemFont(ofSize: 20)
    self.view.addSubview(nameTextView)
  }
  
  func setDefaultConstraints() {
    photoImageView.snp.makeConstraints { (make) -> Void in
      make.width.height.equalTo(200)
      make.centerY.equalTo(view.center.y - 20)
      make.centerX.equalTo(view.center.x)
    }
    nameTextView.snp.makeConstraints { (make) -> Void in
      make.width.equalTo(250)
      make.height.equalTo(55)
      make.centerY.equalTo(view.center.y + 110)
      make.centerX.equalTo(view.center.x)
    }
  }
}
