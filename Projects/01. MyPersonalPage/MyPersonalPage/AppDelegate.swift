//
//  AppDelegate.swift
//  MyPersonalPage
//
//  Created by Rostislav Zapolsky on 4/10/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}
