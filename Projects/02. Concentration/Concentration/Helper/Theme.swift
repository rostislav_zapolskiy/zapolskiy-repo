//
//  Theme.swift
//  Concentration
//
//  Created by Rostislav Zapolsky on 4/22/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

class Theme {
    
    var emoji: [String]
    var backgroundColor: UIColor
    var additionalColor: UIColor
    
    init(emoji: [String], withBackgroundColor backgroundColor: UIColor, withAdditionalColor additionalColor: UIColor) {
        self.emoji = emoji
        self.backgroundColor = backgroundColor
        self.additionalColor = additionalColor
    }
}
