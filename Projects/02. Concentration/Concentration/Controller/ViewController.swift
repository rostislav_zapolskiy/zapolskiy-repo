//
//  ViewController.swift
//  Concentration
//
//  Created by Rostislav Zapolsky on 4/19/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var game: Concentration?
    var currentGameTheme: Theme?
    var gameThemes = [Theme]()
    var emoji = [Int: String]()
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var flipCountLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet var cardButtons: [UIButton]!
    @IBOutlet weak var newGameButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fillGameThemes()
        startNewGame()
    }
    
    @IBAction func touchCard(_ sender: UIButton) {
        guard let cardNumber = cardButtons.firstIndex(of: sender), let unwrappedGame = game else { return }
        unwrappedGame.chooseCard(at: cardNumber)
        updateViewFromModel()
    }
    
    @IBAction func touchStartNewGameButton() {
        startNewGame()
    }
    
    func startNewGame() {
        applyNewTheme()
        game = Concentration(numberOfPairsOfCards: (cardButtons.count + 1) / 2)
        updateViewFromModel()
    }
    
    func fillGameThemes() {
        gameThemes.append(Theme(emoji: ["👻", "🎃", "🦇", "😱", "🙀", "😈", "🍭", "🍬", "🍎", "😨"],
                                withBackgroundColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), withAdditionalColor: #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)))
        gameThemes.append(Theme(emoji: ["🐷", "🐔", "🐢", "🐶", "🐲", "🐇", "🐤", "🐸", "🐝", "🐠"],
                                withBackgroundColor: #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1), withAdditionalColor: #colorLiteral(red: 0.1921568662, green: 0.007843137719, blue: 0.09019608051, alpha: 1)))
        gameThemes.append(Theme(emoji: ["⚽️", "🥊", "🏉", "🏐", "🏓", "🛹", "⛸", "🥎", "🎱", "🏆"],
                                withBackgroundColor: #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1), withAdditionalColor: #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)))
        gameThemes.append(Theme(emoji: ["🥺", "🤠", "🤡", "🤑", "🙄", "🤯", "🤕", "🤥", "😓", "🥴"],
                                withBackgroundColor: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), withAdditionalColor: #colorLiteral(red: 0.3098039329, green: 0.2039215714, blue: 0.03921568766, alpha: 1)))
        gameThemes.append(Theme(emoji: ["🌝", "🌚", "☄️", "💥", "💦", "🌈", "🌞", "❄️", "💫", "🌪"],
                                withBackgroundColor: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), withAdditionalColor: #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1)))
        gameThemes.append(Theme(emoji: ["🥦", "🍆", "🍒", "🍑", "🍌", "🍉", "🍅", "🧀", "🍔", "🌭"],
                                withBackgroundColor: #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1), withAdditionalColor: #colorLiteral(red: 0.1294117719, green: 0.2156862766, blue: 0.06666667014, alpha: 1)))
    }
    
    func applyNewTheme() {
        let themeNumber = Int.random(in: 0..<gameThemes.count)
        currentGameTheme = gameThemes[themeNumber]
        guard let unwrappedCurrentGameTheme = currentGameTheme else { return }
        backgroundView.backgroundColor = unwrappedCurrentGameTheme.backgroundColor
        flipCountLabel.textColor = unwrappedCurrentGameTheme.additionalColor
        scoreLabel.textColor = unwrappedCurrentGameTheme.additionalColor
        newGameButton.setTitleColor(unwrappedCurrentGameTheme.additionalColor, for: .normal)
        
        for cardButton in cardButtons {
            cardButton.backgroundColor = unwrappedCurrentGameTheme.additionalColor
        }
    }
    
    func updateViewFromModel() {
        guard let unwrappedGame = game, let unwrappedCurrentGameTheme = currentGameTheme else { return }
        for index in cardButtons.indices {
            let button = cardButtons[index]
            let card = unwrappedGame.cards[index]
            
            if card.isFaceUp {
                button.setTitle(emoji(for: card), for: .normal)
                button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            } else {
                button.setTitle("", for: .normal)
                button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 0) : unwrappedCurrentGameTheme.additionalColor
            }
        }
        flipCountLabel.text = "Flips: \(unwrappedGame.flipCount)"
        scoreLabel.text = "Score: \(unwrappedGame.score)"
    }
    
    func emoji(for card: Card) -> String {
        guard let unwrappedCurrentGameTheme = currentGameTheme else { return "?" }
        if emoji[card.identifier] == nil, unwrappedCurrentGameTheme.emoji.count > 0 {
            let randomIndex = Int.random(in: 0..<unwrappedCurrentGameTheme.emoji.count)
            emoji[card.identifier] = unwrappedCurrentGameTheme.emoji.remove(at: randomIndex)
        }
        
        return emoji[card.identifier] ?? "?"
    }
}
