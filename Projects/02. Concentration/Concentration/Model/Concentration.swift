//
//  Concentration.swift
//  Concentration
//
//  Created by Rostislav Zapolsky on 4/19/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import Foundation

class Concentration {
    var cards = [Card]()
    var seenCardsIndices = Set<Int>()
    
    var indexOfOneOnlyFaceUpCard: Int?
    var score = 0
    var flipCount = 0
    
    init(numberOfPairsOfCards: Int) {
        for _ in 0..<numberOfPairsOfCards {
            let card = Card()
            cards += [card, card]
        }
        cards.shuffle()
    }
    
    func chooseCard(at index: Int) {
        guard !cards[index].isMatched else { return }
        if let matchIndex = indexOfOneOnlyFaceUpCard {
            guard matchIndex != index else { return }
            compareCards(with: index, to: matchIndex)
            indexOfOneOnlyFaceUpCard = nil
        } else {
            for flipDownIndex in cards.indices {
                cards[flipDownIndex].isFaceUp = false
            }
            indexOfOneOnlyFaceUpCard = index
        }
        flipCount += 1
        cards[index].isFaceUp = true
    }
    
    func compareCards(with cardIndex1: Int, to cardIndex2: Int) {
        if cards[cardIndex2].identifier == cards[cardIndex1].identifier {
            cards[cardIndex2].isMatched = true
            cards[cardIndex1].isMatched = true
            score += 2
        } else {
            for checkingIndex in [cardIndex1, cardIndex2] {
                if seenCardsIndices.contains(checkingIndex) {
                    score -= 1
                } else {
                    seenCardsIndices.insert(checkingIndex)
                }
            }
        }
    }
}
