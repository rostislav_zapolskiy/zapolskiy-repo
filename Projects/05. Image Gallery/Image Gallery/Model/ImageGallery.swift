//
//  ImageGallery.swift
//  Image Gallery
//
//  Created by Rostislav Zapolsky on 6/11/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

struct ImageGallery: Codable {
    
    var imageUrls = [ImageUrl]()
    
    var json: Data? {
        return try? JSONEncoder().encode(self)
    }
    
    init(imageUrls: [ImageUrl]) { 
        self.imageUrls = imageUrls
    }
    
    init?(json: Data) {
        guard let newValue = try? JSONDecoder().decode(ImageGallery.self, from: json) else { return nil }
        self = newValue
    } 
    
}
