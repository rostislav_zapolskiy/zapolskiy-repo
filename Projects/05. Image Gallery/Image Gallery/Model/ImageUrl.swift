//
//  ImageUrl.swift
//  Image Gallery
//
//  Created by Rostislav Zapolsky on 6/18/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import Foundation

struct ImageUrl: Codable {
    
    var url: URL
    var aspectRatio: Double
    
}
