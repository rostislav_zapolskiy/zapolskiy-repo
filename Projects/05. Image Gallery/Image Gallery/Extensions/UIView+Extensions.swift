//
//  UIView+Extensions.swift
//  Image Gallery
//
//  Created by Rostislav Zapolsky on 6/18/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

extension UIView {
    var snapshot: UIImage? {
        UIGraphicsBeginImageContext(bounds.size)
        drawHierarchy(in: bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
