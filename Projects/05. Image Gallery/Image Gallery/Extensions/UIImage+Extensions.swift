//
//  UIImage+Extensions.swift
//  Image Gallery
//
//  Created by Rostislav Zapolsky on 6/18/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

extension UIImage {
    var aspectRatio: Double {
        return Double(self.size.width / self.size.height)
    }
}
