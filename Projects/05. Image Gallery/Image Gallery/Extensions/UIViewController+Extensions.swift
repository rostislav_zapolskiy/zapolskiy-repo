//
//  UIViewController+Extensions.swift
//  Image Gallery
//
//  Created by Rostislav Zapolsky on 6/18/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

extension UIViewController {
    var contents: UIViewController {
        guard let navcon = self as? UINavigationController else { return self }
        return navcon.visibleViewController ?? navcon
    }
}
