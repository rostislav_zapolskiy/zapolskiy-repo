//
//  EmojiArtViewController.swift
//  Image Gallery
//
//  Created by Rostislav Zapolsky on 6/6/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit
import SDWebImage

class ImageGalleryViewController: UIViewController, UICollectionViewDelegate {
    
    // MARK: Properties
    var imageGallery: ImageGallery? {
        get {
            return ImageGallery(imageUrls: imageUrls)
        } 
        set {
            imageViews = []
            imageUrls = []
            
            guard let modelUrls = newValue?.imageUrls else { return }
            for modelUrl in modelUrls {
                var image = SDImageCache.shared.imageFromDiskCache(forKey: modelUrl.url.absoluteString)
                if image == nil {
                    guard let data = try? Data(contentsOf: modelUrl.url) else { continue }
                    image = UIImage(data: data)
                }
                guard let definitelyimage = image else { continue }
                imageViews.append(definitelyimage)
                imageUrls.append(ImageUrl(url: modelUrl.url,
                                          aspectRatio: definitelyimage.aspectRatio))
            }
            imageGalleryCollectionView.reloadData()
        }
    }
    
    var imageViews: [UIImage] = []
    var imageUrls: [ImageUrl] = []
    
    var document: ImageGalleryDocument?
    
    @IBOutlet weak var imageGalleryCollectionView: ImageGalleryCollectionView! {
        didSet {
            imageGalleryCollectionView.addInteraction(UIDropInteraction(delegate: self))
            imageGalleryCollectionView.dataSource = self
            imageGalleryCollectionView.delegate = self
            imageGalleryCollectionView.dragDelegate = self
            imageGalleryCollectionView.dropDelegate = self
            imageGalleryCollectionView.dragInteractionEnabled = true
        }
    }
        
    // MARK: Document save methods
    func documentChanged() {
        guard let document =  document else { return }
        document.imageGallery = imageGallery
        if imageGallery != nil {
            document.updateChangeCount(.done)
        }
    }
    
    @IBAction func close(_ sender: Any) {
        guard let document = document else { return }
        if document.imageGallery != nil {
            document.thumbnail = imageGalleryCollectionView.snapshot
        }
        
        dismiss(animated: true) {
            document.close()
        }
    }
    
    // MARK: Starting methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let document = document else { return }
        document.open { success in
            if success {
                self.title = document.localizedName
                self.imageGallery = document.imageGallery
            }
        }
        
        addGestureRecognizers()
        imageGalleryCollectionView.calculateBasicCellWidth()
    }
    
    func addGestureRecognizers() {
        let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self,
                                                              action: #selector(resizeCellWidth(sender:)))
        view.addGestureRecognizer(pinchGestureRecognizer)
    }
    
    @objc func resizeCellWidth(sender: UIPinchGestureRecognizer) {
        guard case .changed = sender.state,
            let predefinedWidth = imageGalleryCollectionView.predefinedWidth else { return }
        imageGalleryCollectionView.predefinedWidth = predefinedWidth * sender.scale
        sender.scale = 1.0
        imageGalleryCollectionView.reloadData()
    }
    
    // MARK: Starting methods
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let imageCell = sender as? ImageViewCell,
            let indexPath = imageGalleryCollectionView.indexPath(for: imageCell),
            let detailImageViewController = segue.destination as? DetailImageViewController else {
                return
        }
        detailImageViewController.imageURL = imageUrls[indexPath.item].url
    }
}

extension ImageGalleryViewController: UICollectionViewDropDelegate, UIDropInteractionDelegate {
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        let destinationIndexPath = coordinator.destinationIndexPath ?? IndexPath(item: 0, section: 0)
        for item in coordinator.items {
            if let sourceIndexPath = item.sourceIndexPath {
                changeImagePlace(with: coordinator, from: sourceIndexPath, to: destinationIndexPath, of: item)
            } else {
                downloadImages(with: coordinator, to: destinationIndexPath, of: item)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        let isSelf = session.localDragSession?.localContext as? UICollectionView == collectionView
        if isSelf {
            return session.canLoadObjects(ofClass: UIImage.self)
        } else {
            return session.canLoadObjects(ofClass: UIImage.self) && session.canLoadObjects(ofClass: NSURL.self)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        let isSelf = session.localDragSession?.localContext as? UICollectionView == collectionView
        return UICollectionViewDropProposal(operation: isSelf ? .move : .copy, intent: .insertAtDestinationIndexPath )
    }
    
    func downloadImages(with coordinator: UICollectionViewDropCoordinator, to indexPath: IndexPath, of item: UICollectionViewDropItem) {
        
        var imageUrl: URL?
        var imageAspectRatio: Double?
        
        item.dragItem.itemProvider.loadObject(ofClass: UIImage.self, completionHandler: { (provider, _) in
            DispatchQueue.main.async {
                guard let image = provider as? UIImage else { return }
                imageAspectRatio = image.aspectRatio
                self.imageViews.insert(image, at: indexPath.item)
                self.imageGalleryCollectionView.reloadData()
            }
        })
        
        item.dragItem.itemProvider.loadObject(ofClass: NSURL.self) { (provider, _) in
            DispatchQueue.main.async {
                guard let url = provider as? URL else { return }
                imageUrl = url
                guard let imageUrl = imageUrl, let imageAspectRatio = imageAspectRatio else { return }
                self.imageUrls.insert(ImageUrl(url: imageUrl, aspectRatio: imageAspectRatio), at: indexPath.item)
                self.imageGalleryCollectionView.reloadData()
                self.documentChanged()
            }
        }
    }
    
    func changeImagePlace(with coordinator: UICollectionViewDropCoordinator, from indexPathFrom: IndexPath, to indexPathTo: IndexPath, of item: UICollectionViewDropItem) {
        
        guard let image = item.dragItem.localObject as? UIImage else { return }
        imageGalleryCollectionView.performBatchUpdates({ () -> Void in
            imageViews.remove(at: indexPathFrom.item)
            imageUrls.insert(imageUrls.remove(at: indexPathFrom.item), at: indexPathTo.item)
            imageViews.insert(image, at: indexPathTo.item)
            imageGalleryCollectionView.deleteItems(at: [indexPathFrom])
            imageGalleryCollectionView.insertItems(at: [indexPathTo])
        }, completion: { _ in
            self.documentChanged()
        })
        coordinator.drop(item.dragItem, toItemAt: indexPathTo)
    }
}

extension ImageGalleryViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageGallery?.imageUrls.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath)
        guard imageViews.count == imageGallery?.imageUrls.count else { return cell }
        if let cell = cell as? ImageViewCell {
            cell.imageURL = imageUrls[indexPath.item].url
        }
        return cell
    }
}

extension ImageGalleryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let predefinedWidth = imageGalleryCollectionView.predefinedWidth else { return CGSize.zero }
        return CGSize(width: predefinedWidth, height: predefinedWidth / CGFloat(imageUrls[indexPath.item].aspectRatio))
    }
}

extension ImageGalleryViewController: UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        session.localContext = collectionView
        return dragItems(at: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo session: UIDragSession, at indexPath: IndexPath, point: CGPoint) -> [UIDragItem] {
        return dragItems(at: indexPath)
    }
    
    private func dragItems(at indexPath: IndexPath) -> [UIDragItem] {
        guard let image = (imageGalleryCollectionView.cellForItem(at: indexPath) as?
            ImageViewCell)?.imageView.image else { return [] }
        
        let dragItem = UIDragItem(itemProvider: NSItemProvider(object: image))
        dragItem.localObject = image
        return [dragItem]
    }
}
