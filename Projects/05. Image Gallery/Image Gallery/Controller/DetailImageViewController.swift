//
//  DetailImageViewController.swift
//  Image Gallery
//
//  Created by Rostislav Zapolsky on 6/18/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//
//  Stolen from DED

import UIKit

class DetailImageViewController: UIViewController, UIScrollViewDelegate {
    
    var imageURL: URL? {
        didSet {
            image = nil
            if view.window != nil {
                fetchImage()
            }
        }
    }
    
    private var image: UIImage? {
        get {
            return imageView.image
        }
        set {
            imageView.image = newValue
            imageView.sizeToFit()
            scrollView?.contentSize = imageView.frame.size
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if imageView.image == nil {
            fetchImage()
        }
    }
    
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.minimumZoomScale = 1/25
            scrollView.maximumZoomScale = 1.0
            scrollView.delegate = self
            scrollView.addSubview(imageView)
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    var imageView  = UIImageView()
    
    private func fetchImage() {
        guard let url = imageURL else { return }
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            let urlContents = try? Data(contentsOf: url)
            DispatchQueue.main.async {
                guard let imageData = urlContents, url == self?.imageURL else { return }
                self?.image = UIImage(data: imageData)
            }
        }
    }
}
