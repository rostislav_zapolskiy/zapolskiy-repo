//
//  EmojiArtView.swift
//  Image Gallery
//
//  Created by Rostislav Zapolsky on 6/6/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

class DetailImageView: UIView, UIDropInteractionDelegate {
    
    var backgroundImage: UIImage? { didSet { setNeedsDisplay() } }
    
    // MARK: Inits
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        addInteraction(UIDropInteraction(delegate: self))
    }
    
    // MARK: Drop
    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: NSAttributedString.self)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        return UIDropProposal(operation: .copy)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        session.loadObjects(ofClass: NSAttributedString.self) { providers in
            let dropPoint = session.location(in: self)
            for attributedString in providers as? [NSAttributedString] ?? [] {
                self.addLabel(with: attributedString, centeredIn: dropPoint)
            }
        }
    }
    
    private func addLabel(with attributedString: NSAttributedString, centeredIn point: CGPoint) {
        let label = UILabel( )
        label.backgroundColor = .clear
        label.attributedText = attributedString
        label.sizeToFit()
        label.center = point
        addSubview(label)
    }
    
    override func draw(_ rect: CGRect) {
        backgroundImage?.draw(in: bounds)
    }
}
