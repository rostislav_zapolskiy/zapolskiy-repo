//
//  ImageViewCell.swift
//  DED EmojiArt
//
//  Created by Rostislav Zapolsky on 6/7/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit
import SDWebImage

class ImageViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageDownloadingIndicatorView: UIActivityIndicatorView!
    
    var imageURL: URL? {
        didSet {
            imageDownloadingIndicatorView.startAnimating()
            imageDownloadingIndicatorView.isHidden = false
            
            imageView.sd_setImage(with: self.imageURL) { (_, _, _, _) in
                self.imageDownloadingIndicatorView.stopAnimating()
                self.imageDownloadingIndicatorView.isHidden = true
            }
        }
    }
}
