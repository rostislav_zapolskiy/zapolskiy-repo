//
//  ImageGalleryCollectionView.swift
//  Image Gallery
//
//  Created by Rostislav Zapolsky on 6/19/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

class ImageGalleryCollectionView: UICollectionView {

    var predefinedWidth: CGFloat?
    
    func calculateBasicCellWidth() {
        
        var flowLayout: UICollectionViewFlowLayout? { return collectionViewLayout as? UICollectionViewFlowLayout }
        
        var boundsCollectionWidth: CGFloat { return bounds.width }
        var gapItems: CGFloat? {
            guard let flowLayout = flowLayout else { return nil }
            return (flowLayout.minimumInteritemSpacing) * CGFloat((Constants.columnCount - 1.0))
        }
        var gapSections: CGFloat? {
            guard let flowLayout = flowLayout else { return nil }
            return flowLayout.sectionInset.right * 2.0
        }
        var scale: CGFloat = 1 {
            didSet {
                collectionViewLayout.invalidateLayout()
            }
        }
        
        guard let unwrappedGapItems = gapItems, let unwrappedGapSections = gapSections else { return }
        let width = floor((boundsCollectionWidth - unwrappedGapItems - unwrappedGapSections)
            / CGFloat(Constants.columnCount)) * scale
        
        predefinedWidth = min(max(width, boundsCollectionWidth * Constants.minWidthRation), boundsCollectionWidth)
    }
}

extension ImageGalleryCollectionView {
    private struct Constants {
        static let columnCount = 3.0
        static let minWidthRation = CGFloat(0.03)
    }
}
