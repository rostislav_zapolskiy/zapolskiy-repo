//
//  ViewController.swift
//  Set
//
//  Created by Rostislav Zapolsky on 4/23/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: Variables
    var game: SetGame?
    lazy var cardsGrid = Grid(layout: .aspectRatio(CardDefaultPropetries.cardAspectRatio),
                              frame: cardButtons.bounds)

    @IBOutlet weak var cardButtons: UIView!
    @IBOutlet weak var dealCardsButton: UIButton!
    @IBOutlet weak var pointsLabel: UILabel!
    
    // MARK: Overriden
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addGestureRecognizers()
        startNewGame()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateViewFromModel()
    }
    
    // MARK: Actions
    @IBAction func touchStartGameButton() {
        startNewGame()
        updateViewFromModel()
    }
    
    @IBAction func touchDealCardsButton(_ sender: UIButton) {
    	dealCards()
    }
    
    @IBAction func findSetButton(_ sender: UIButton) {
        guard let unwrappedGame = game else { return }
        
        unwrappedGame.findSet(withCardsSelecting: true)
        updateViewFromModel()
        checkIfGameEnded()
    }
    
    // MARK: Gestures
    @objc func touchCardEvent(_ sender: UIButton) {
        guard let unwrappedGame = game,
            let indexOfTouchedCard = cardButtons.subviews.firstIndex(of: sender) else { return }
        
        unwrappedGame.touchCard(at: indexOfTouchedCard)
        updateViewFromModel()
        checkIfGameEnded()
    }
    
    @objc func reshuffleCardsEvent(_ sender: UIRotationGestureRecognizer) {
        guard let unwrappedGame = game,
            case .ended = sender.state else {
                return
        }
        unwrappedGame.reshuffleCards()
        updateViewFromModel()
    }
    
    @objc func dealCardsEvent(_ sender: UISwipeGestureRecognizer) {
        dealCards()
    }
    
    // MARK: Support
    func startNewGame() {
        game = SetGame()
    }
    
    func addGestureRecognizers() {
        let swipeGestureRecognizer = UISwipeGestureRecognizer(target: self,
                                                              action: #selector(dealCardsEvent(_:)))
        swipeGestureRecognizer.direction = .down
        let rotateGestureRecognizer = UIRotationGestureRecognizer(target: self,
                                                                  action: #selector(reshuffleCardsEvent(_:)))
        
        view.addGestureRecognizer(swipeGestureRecognizer)
        view.addGestureRecognizer(rotateGestureRecognizer)
    }
    
    func checkIfGameEnded() {
        guard let unwrappedGame = game else { return }
        
        if !unwrappedGame.findSet(withCardsSelecting: false) && unwrappedGame.remainingCards.isEmpty {
            unwrappedGame.updateGameAfterFindingSet()
            updateViewFromModel()
            finishGame(with: unwrappedGame.totalPoints)
        }
    }
    
    func dealCards() {
        guard let unwrappedGame = game else { return }
        
        unwrappedGame.dealCardsOnButtonTouch()
        updateViewFromModel()
    }
    
    func finishGame(with points: Int) {
        let alert = UIAlertController(title: "Game over!",
                                      message: "You scored \(points) points",
            preferredStyle: .alert)
        let action = UIAlertAction(title: "OK",
                                   style: .default,
                                   handler: { _ in
                                    self.startNewGame()
                                    self.updateViewFromModel()
        })
        present(alert, animated: true, completion: nil)
        alert.addAction(action)
    }
    
    // MARK: View update
    func updateViewFromModel() {
        guard let unwrappedGame = game else { return }
        
        cardsGrid.frame = cardButtons.bounds
        cardsGrid.cellCount = unwrappedGame.ingameCards.count
        
        for cardView in cardButtons.subviews {
            cardView.removeFromSuperview()
        }
        
        for index in 0..<cardsGrid.cellCount {
            guard let cellFrame = cardsGrid[index] else { break }
            var cardView = CardButton(frame: cellFrame.insetBy(dx: CardDefaultPropetries.cardIndent,
                                                               dy: CardDefaultPropetries.cardIndent))
            cardView.addTarget(self, action: #selector(touchCardEvent(_:)), for: .touchUpInside)
            createCardView(for: unwrappedGame.ingameCards[index], with: &cardView)
            
            cardButtons.addSubview(cardView)
        }
        dealCardsButton.isHidden = unwrappedGame.remainingCards.isEmpty
        pointsLabel.text = "Points: \(unwrappedGame.totalPoints)"
    }
    
    func createCardView(for card: Card, with cardView: inout CardButton) {
        guard let unwrappedGame = game,
            let buttonSymbol = card.dimensionIdentifiers[0].value as? Symbols,
            let buttonSymbolQuantity = card.dimensionIdentifiers[1].value as? Int,
            let buttonSymbolColor = card.dimensionIdentifiers[2].value as? UIColor,
            let buttonSymbolTransparency = card.dimensionIdentifiers[3].value as? Transparency else {
                cardView.setCardButtonBorder(borderColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                return
        }
        cardView.setCardButtonImage(symbol: buttonSymbol, quantity: buttonSymbolQuantity,
                                    color: buttonSymbolColor, transparency: buttonSymbolTransparency)
        
        var buttonBorderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        if unwrappedGame.chosenCards.contains(card) {
            buttonBorderColor = unwrappedGame.chosenCardsQuantityEqualsSetQuantity()
                && unwrappedGame.isCardsAreSet() ? #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1) : #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        }
        cardView.setCardButtonBorder(borderColor: buttonBorderColor)
    }
}

extension ViewController {
    private struct CardDefaultPropetries {
        static let cardAspectRatio: CGFloat = 8.0 / 5.0
        static let cardIndent: CGFloat = 0.5
    }
}
