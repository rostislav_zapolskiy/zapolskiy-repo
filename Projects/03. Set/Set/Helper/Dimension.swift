//
//  Dimensions.swift
//  Set
//
//  Created by Rostislav Zapolsky on 4/29/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

enum Dimension: Hashable {
    case symbol(identifier: Int)
    case symbolQuantity(identifier: Int)
    case color(identifier: Int)
    case transparency(identifier: Int)
    
    var value: Any {
        switch self {
        case .symbol(let index):
            switch index {
            case 0:
                return Symbols.diamond
            case 1:
                return Symbols.oval
            default:
                return Symbols.squiggle
            }
        case .symbolQuantity(let index):
            switch index {
            case 0:
                return 1
            case 1:
                return 2
            default:
                return 3
            }
        case .color(let index):
            switch index {
            case 0:
                return #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            case 1:
                return UIColor.red
            default:
                return UIColor.purple
            }
        case .transparency(let index):
            switch index {
            case 0:
                return Transparency.solid
            case 1:
                return Transparency.striped
            default:
                return Transparency.unfilled
            }
        }
    }
}

enum Symbols {
    case squiggle, diamond, oval
}

enum Transparency {
    case solid, striped, unfilled
}
