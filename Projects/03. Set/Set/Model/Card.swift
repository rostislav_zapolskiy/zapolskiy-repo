//
//  Card.swift
//  Set
//
//  Created by Rostislav Zapolsky on 4/23/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import Foundation

struct Card: Hashable {
    let dimensionIdentifiers: [Dimension]
    
    init(dimensionIdentifiers: [Dimension]) {
        self.dimensionIdentifiers = dimensionIdentifiers
    }
    
    static func == (lhs: Card, rhs: Card) -> Bool {
        return lhs.dimensionIdentifiers == rhs.dimensionIdentifiers
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(dimensionIdentifiers)
    }
}
