//
//  Set.swift
//  Set
//
//  Created by Rostislav Zapolsky on 4/23/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import Foundation
import Combinatorics

class SetGame {
    
    // MARK: Variables
    var totalPoints = 0
    var setWasFoundByHint = false
    var remainingCards = [Card]()
    var ingameCards = [Card]()
    var chosenCards = Set<Card>()
    
    // MARK: Initialization
    init(withDimensions dimensions: Int = GameDefaultParameters.dimensionsQuantity,
         withDimensionVariations dimensionVariations: Int = GameDefaultParameters.dimensionVariationsQuantity) {
        var currentCardId = [Int].init(repeating: 0, count: dimensions)
        let numberOfCards = Int(pow(Double(dimensionVariations), Double(dimensions)))
        
        for _ in 0..<numberOfCards {
            var upgradingId = dimensions - 1
            if currentCardId[upgradingId] == dimensionVariations {
                while currentCardId[upgradingId] == dimensionVariations {
                    currentCardId[upgradingId] = 0
                    upgradingId -= 1
                    currentCardId[upgradingId] += 1
                }
            }
            remainingCards.append(Card(dimensionIdentifiers: [Dimension.symbol(identifier: currentCardId[0]),
                                                              Dimension.symbolQuantity(identifier: currentCardId[1]),
                                                              Dimension.color(identifier: currentCardId[2]),
                                                              Dimension.transparency(identifier: currentCardId[3])]))
            
            currentCardId[dimensions - 1] += 1
        }
        dealCards(of: GameDefaultParameters.dealingCardsOnStart)
    }
    
    // MARK: Support
    func touchCard(at cardIndex: Int) {
        let touchedCard = ingameCards[cardIndex]
        
        if chosenCards.count == GameDefaultParameters.cardsInOneSet {
            if isCardsAreSet() {
                updateGameAfterFindingSet()
            } else {
                totalPoints += GameDefaultParameters.changingPointsWhenSetNotFound
            }
            chosenCards.removeAll()
            guard ingameCards.contains(touchedCard) else { return }
            chosenCards.insert(touchedCard)
        } else {
            if chosenCards.contains(touchedCard) {
                chosenCards.remove(touchedCard)
            } else {
                chosenCards.insert(touchedCard)
            }
        }        
    }
    
    func reshuffleCards() {
        let countOfIngameCards = ingameCards.count
        chosenCards.removeAll()
        
        remainingCards += ingameCards
        remainingCards.shuffle()
        ingameCards.removeAll()
        
        for _ in 0..<countOfIngameCards {
        	ingameCards.append(remainingCards.removeFirst())
        }
        totalPoints += GameDefaultParameters.changingPointsWhenReshufflingCards
    }
    
    func updateGameAfterFindingSet() {
        if setWasFoundByHint {
            totalPoints += GameDefaultParameters.changingPointsWhenSetFoundByHint
            setWasFoundByHint = false
        } else {
            totalPoints += GameDefaultParameters.changingPointsWhenSetFoundWithoutHint
        }
        for chosenCard in chosenCards {
            guard let indexOfChosenCard = ingameCards.firstIndex(of: chosenCard) else { return }
            dealCard(at: indexOfChosenCard)
        }
        chosenCards.removeAll()
    }
    
    func chosenCardsQuantityEqualsSetQuantity() -> Bool {
        return chosenCards.count == GameDefaultParameters.cardsInOneSet
    }
    
    // MARK: Card dealing
    func dealCardsOnButtonTouch() {
        dealCards(of: GameDefaultParameters.dealingCardsOnButtonTouch)
    }
    
    func dealCards(of quantity: Int) {
        for _ in 0..<quantity {
            guard let randomCard = remainingCards.randomElement() else { return }
            
            ingameCards.append(randomCard)
            remainingCards.removeAll(where: { $0 == randomCard })
        }
    }
    
    func dealCard(at index: Int) {
        guard let randomCard = remainingCards.randomElement() else {
        	ingameCards.remove(at: index)
            return
        }
        ingameCards[index] = randomCard
        remainingCards.removeAll(where: { $0 == randomCard })
    }
    
    // MARK: Set finding
    @discardableResult
    func findSet(withCardsSelecting: Bool) -> Bool {
        if withCardsSelecting, isCardsAreSet() {
            updateGameAfterFindingSet()
        }
        
        let cardCombinations =
            Combinatorics.combinationsWithoutRepetitionFrom(ingameCards, taking: GameDefaultParameters.cardsInOneSet)
        for cardCombination in cardCombinations {
            guard isCardsAreSet(checkingCards: Set(cardCombination)) else { continue }
            if withCardsSelecting {
                chosenCards = Set(cardCombination)
                setWasFoundByHint = true
            }
            return true
        }
        return false
    }
    
    func isCardsAreSet(checkingCards: Set<Card>? = nil) -> Bool {
        let checkingCards = checkingCards ?? chosenCards
        var isSet = true
        guard let firstCard = checkingCards.first else { return false }
        for index in firstCard.dimensionIdentifiers.indices {
            var sumOfComparingIdentifiers = 0
            for checkingCard in checkingCards {
                let currentDimension = checkingCard.dimensionIdentifiers[index]
                switch currentDimension {
                case .color(let identifierValue):
                    sumOfComparingIdentifiers += identifierValue
                case .symbol(let identifierValue):
                    sumOfComparingIdentifiers += identifierValue
                case .symbolQuantity(let identifierValue):
                    sumOfComparingIdentifiers += identifierValue
                case .transparency(let identifierValue):
                    sumOfComparingIdentifiers += identifierValue
                }
            }
            
            isSet = sumOfComparingIdentifiers % GameDefaultParameters.cardsInOneSet == 0
            
            guard isSet else { break }
        }
        return isSet
    }    
}

extension SetGame {
    private struct GameDefaultParameters {
        static let dimensionsQuantity = 4
        static let dimensionVariationsQuantity = 3
        static let cardsInOneSet = 3
        static let dealingCardsOnStart = 12
        static let dealingCardsOnButtonTouch = 3
        
        static let changingPointsWhenSetNotFound = -3
        static let changingPointsWhenSetFoundByHint = -5
        static let changingPointsWhenSetFoundWithoutHint = 5
        static let changingPointsWhenReshufflingCards = -5
    }
}
